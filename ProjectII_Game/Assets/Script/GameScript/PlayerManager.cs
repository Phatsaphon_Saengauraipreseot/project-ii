﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    CharacterController characterController;

    //ตัวแปร
    [SerializeField] private float speed;//ความเร็ว
    [SerializeField] private float jumpSpeed;//ความเร็วในการกระโดด
    [SerializeField] private float gravity;//แรงโน้มถ่วง   
    private Vector3 moveDirection = Vector3.zero;
    //กำหนดค่าของเสียง
    [SerializeField] private AudioClip playerJumpSound;
    [SerializeField] private float playerJumpSoundVolume = 0.1f;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        //การเคลื่อนที่ของตัวละคร
        if (characterController.isGrounded)
        {
            //Move ซ้าย - ขวา
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f);
            moveDirection *= speed;

            //กระโดด
            if (Input.GetButton("Jump"))
            {
                //เล่นเสียง
                AudioSource.PlayClipAtPoint(playerJumpSound, Camera.main.transform.position, playerJumpSoundVolume);
                moveDirection.y = jumpSpeed;
            }
        }
        //คำนวณการเคลื่อน ให้มีผลกับแรงโน้มถ่วง
        moveDirection.y -= gravity * Time.deltaTime;
       characterController.Move(moveDirection * Time.deltaTime);


    }

   







}//

