﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private GameMaster gm;

    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
    }

    //กำหนดตำแหน่งที่ Player จะเกิด เมื่อเริ่มเกม หรือ กด " R " เพื่อกลับมายังตำแน่งนี้  
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gm.lastCheckPointPosition = transform.position;
        }

    }
}
