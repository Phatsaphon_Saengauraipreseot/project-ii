﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{

    private static GameMaster instance;
    //ตำแหน่งของตัวละคร
    public Vector3 lastCheckPointPosition;


    private void Awake()
    {
        instance = this;
    }

    public void Update()
    {
        //ปิดเกม
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
