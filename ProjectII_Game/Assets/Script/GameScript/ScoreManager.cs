﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    //ตัวแปรของคะแนน
    public int score;
    public Text scoreText;

    //กำหนดค่าของเสียง
    [SerializeField] private AudioClip coinSound;
    [SerializeField] private float coinSoundVolume = 0.005f;

    void Start()
    {
        //เริ่มเซตคะแนนให้เป็น 0 
        score = 0;

    }  

    //Player มาชนกับ Coin 
    private void OnTriggerEnter(Collider other)
    {
        //ถ้า Player มาชนกับ Object ที่มี Tag ว่า Coin ให้เพิ่มคะแน
        if (other.tag == "Coin")
        {
            //เล่นเสียง
            AudioSource.PlayClipAtPoint(coinSound, Camera.main.transform.position, coinSoundVolume);
            score++;
            scoreText.text = "Score : " + score;
            Destroy(other.gameObject);             
        }
    }
}

