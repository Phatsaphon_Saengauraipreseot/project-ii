﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngularVelocity : MonoBehaviour
{
    [SerializeField] private Vector3 angularVelocity;

    Rigidbody rb;

    void Start()
    {
        // ความเร็วเชิงมุม

        rb = GetComponent<Rigidbody>();
        rb.angularVelocity = angularVelocity;
    }

   

   
}
