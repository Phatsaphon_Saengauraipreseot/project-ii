﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatforms : MonoBehaviour
{

    public GameObject platforms;

    public float moveSpeed;

    public Transform currentPoint;

    public Transform[] points;

    public int pointSelection;
    
    void Start()
    {
        //จำนวนของตำแหน่งที่เราต้องการ
        currentPoint = points[pointSelection];
    }


    void Update()
    {
        //สั่งให้ Object เคลื่อนไปยังตำแหน่งที่กำหนด
        platforms.transform.position = Vector3.MoveTowards(platforms.transform.position, currentPoint.position,
            Time.deltaTime * moveSpeed);

        if (platforms.transform.position == currentPoint.position)
        {
            pointSelection++;

            if (pointSelection == points.Length)
            {
                pointSelection = 0;
            }

            currentPoint = points[pointSelection];

        }


    }
}
