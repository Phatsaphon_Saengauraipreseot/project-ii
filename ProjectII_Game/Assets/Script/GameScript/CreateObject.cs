﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObject : MonoBehaviour
{
    public Transform Spawnpoint;
    public Rigidbody Prefab ;

    //สร้างหินมาเป็นอุปสรรค
void OnTriggerEnter() 
    {
        Rigidbody Rock;
        Rock = Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation) as Rigidbody;

        if (Rock.CompareTag("RockDestroy"))
        {
            Destroy(this.gameObject);
            return;
        }
    }

     


}
